const io = require('../socket');

exports.emit = (channel, action, data) => {
  io.getIO().emit(channel, {
    action: action,
    data: data
  });
}