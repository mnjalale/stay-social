const express = require('express');

const feedController = require('../controllers/feed');
const feedValidator = require('../middleware/routeValidators/feed');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

router.get('/posts', isAuth, feedController.getPosts);

router.post('/post', isAuth, feedValidator.validatePost, feedController.createPost);

router.get('/post/:postId', isAuth, feedController.getPost);

router.put('/post/:postId', isAuth, feedValidator.validateUpdate, feedController.updatePost);

router.delete('/post/:postId', isAuth, feedController.deletePost);

router.get('/status', isAuth, feedController.getStatus);

router.post('/status', isAuth, feedController.postStatus);

module.exports = router;