const express = require('express');
const authValidator = require('../middleware/routeValidators/auth');
const authController = require('../controllers/auth');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

router.put('/signup', authValidator.validateSignup, authController.signup);

router.post('/login', authValidator.validateLogin, authController.login);

module.exports = router;