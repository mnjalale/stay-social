const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');

const feedRoutes = require('./routes/feed');
const authRoutes = require('./routes/auth');

const fileUpload = require('./middleware/fileUpload');

const app = express();

app.use(bodyParser.json());
app.use(fileUpload);
app.use('/images', express.static(path.join(__dirname, 'images')));

// Enable CORS
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.use('/feed', feedRoutes);
app.use('/auth', authRoutes);

app.use((error, req, res, next) => {
  console.log(error);
  const status = error.statusCode || 500;
  const message = error.message;
  const data = error.data;
  res
    .status(status)
    .json({
      message: message,
      data: data
    });
})

const server = app.listen(8080);
//setup socket.io connection
const io = require('./socket').init(server);
io.on('connection', socket => {
  console.log('Client connected');
});