const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator/check');
const jwt = require('jsonwebtoken');

const db = require('../models/index');
const User = db['User'];


exports.signup = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Validation failed.');
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }
  const email = req.body.email;
  const name = req.body.name;
  const password = req.body.password;

  try {
    const hashedPassword = await  bcrypt.hash(password, 12);

    const user = await User.create({
      email: email,
      name: name,
      password: hashedPassword
    });

    res
      .status(201)
      .json({
        message: "User created",
        userId: user.id
      });
  } catch (error) {
    catchControllerErrors(error, next);
  }
};

exports.login = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = new Error('Validation errors occurred');
    error.statusCode = 422;
    error.data = errors.array();
    throw error;
  }

  const email = req.body.email;
  const password = req.body.password;

  try {
    const user = await User.findOne({
      where: {
        email: email
      }
    });

    if (!user) {
      const error = new Error('Invalid email or password');
      error.statusCode = 401;
      throw error;
    }


    const isValid = await bcrypt.compare(password, user.password);

    if (!isValid) {
      const error = new Error('Invalid email or password');
      error.statusCode = 401;
      throw error;
    }

    // Generate JSON Web Token
    const token = jwt.sign({
      email: user.email,
      userId: user.id.toString()
    }, 'secret', // This should be a very long secret
      {
        expiresIn: '1h'
      });

    res.status(200)
      .json({
        token: token,
        userId: user.id
      });
  } catch (error) {
    catchControllerErrors(error, next);
  }

};

// Helper functions
const catchControllerErrors = (error, next) => {
  if (!error.statusCode) {
    error.statusCode = 500;
  }
  next(error);
};