const fs = require('fs');
const path = require('path');

const {validationResult} = require('express-validator/check');

const io = require('../utils/socket');
const db = require('../models/index');
const Post = db['Post'];
const User = db['User'];

exports.getPosts = async (req, res, next) => {
  const currentPage = parseInt(req.query.page) || 1;
  const itemsPerPage = 2;

  try {
    const totalItems = await Post.count();

    const posts = await Post.findAll({
      offset: (currentPage - 1) * itemsPerPage,
      limit: itemsPerPage,
      include: [User],
      order: [['createdAt', 'DESC']]
    });

    const modifiedPosts = posts.map(post => {
      return getReturnPost(post, post.User);
    });

    res
      .status(200)
      .json({
        message: 'Fetched posts successfully',
        posts: modifiedPosts,
        totalItems: totalItems
      });
  } catch (error) {
    catchControllerErrors(error, next);
  }
};

exports.createPost = async (req, res, next) => {

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const errorMessageArray = errors.array().map(error => error.msg);
    const errorMessage = errorMessageArray
      .reduce((fullMessage, message) => {
        if (fullMessage) {
          return fullMessage + '\n' + message;
        } else {
          return message;
        }
      });

    const error = new Error(errorMessage);
    error.statusCode = 422;
    throw error;
  }

  if (!req.file) {
    const error = new Error('No image provided');
    error.statusCode = 422;
    throw error;
  }

  const imageUrl = req.file.path.replace('\\', '/');
  const title = req.body.title;
  const content = req.body.content;

  // Create post in db
  try {
    const creator = await  User.findByPk(req.userId);
    const post = await creator.createPost({
      title: title,
      content: content,
      imageUrl: imageUrl,
    });

    io.emit('posts', 'create', getReturnPost(post, creator));

    res
      .status(201)
      .json({
        message: 'Post created successfully!',
        post: getReturnPost(post, creator)
      });
  } catch (error) {
    catchControllerErrors(error, next);
  }

};

exports.updatePost = async (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const errorMessageArray = errors.array().map(error => error.msg);
    const errorMessage = errorMessageArray
      .reduce((fullMessage, message) => {
        if (fullMessage) {
          return fullMessage + '\n' + message;
        } else {
          return message;
        }
      });

    const error = new Error(errorMessage);
    error.statusCode = 422;
    throw error;
  }

  const postId = req.params.postId;
  const title = req.body.title;
  const content = req.body.content;
  let imageUrl = req.body.image;
  if (req.file) {
    imageUrl = req.file.path;
  }

  if (!imageUrl) {
    const error = new Error('No file picked.');
    error.statusCode = 422;
    throw error;
  }

  try {
    let post = await Post.findByPk(postId, {
      include: [User]
    });

    if (!post) {
      const error = new Error('Could not find post.');
      error.statusCode = 404;
      throw error;
    }

    if (post.userId !== req.userId) {
      const error = new Error('Not Authorized');
      error.statusCode = 403;
      throw error;
    }

    imageUrl = imageUrl.replace('\\', '/');
    if (imageUrl !== post.imageUrl) {
      clearImage(post.imageUrl);
    }

    post.title = title;
    post.content = content;
    post.imageUrl = imageUrl;

    post = await post.save();

    io.emit('posts', 'update', getReturnPost(post, post.User));

    res.status(200).json({
      message: 'Post updated',
      post: getReturnPost(post, post.User)
    });
  } catch (error) {
    catchControllerErrors(error, next);
  }
};

exports.deletePost = async (req, res, next) => {

  const postId = req.params.postId;

  try {
    const post = await Post.findByPk(postId);

    if (!post) {
      const error = new Error('Could not find post.');
      error.statusCode = 404;
      throw error;
    }

    if (post.userId !== req.userId) {
      const error = new Error('Not Authorized');
      error.statusCode = 403;
      throw error;
    }

    if (post.imageUrl) {
      clearImage(post.imageUrl);
    }

    const result = await post.destroy();

    io.emit('posts', 'delete', postId);

    res.status(200).json({
      message: 'Post deleted successfully'
    });
  } catch (error) {
    catchControllerErrors(error, next);
  }
};

exports.getPost = async (req, res, next) => {
  const postId = req.params.postId;

  try {
    const post = await Post.findByPk(postId, {
      include: [User]
    });

    if (!post) {
      const error = new Error('Could not find post');
      error.statusCode = 404;
      throw error;
    }

    res.status(200).json({
      post: getReturnPost(post, post.User)
    });
  } catch (error) {
    catchControllerErrors(error, next);
  }
};

exports.getStatus = async (req, res, next) => {
  const userId = req.userId;

  try {
    const user = await User.findByPk(userId);
    if (!user) {
      const error = new Error('User not found');
      error.statusCode = 404;
      throw error;
    }

    res
      .status(200)
      .json({
        status: user.status
      });
  } catch (error) {
    catchControllerErrors(error, next);
  }
};

exports.postStatus = async (req, res, next) => {
  const status = req.body.status;
  const userId = req.userId;

  try {
    let user = await User.findByPk(userId);

    if (!user) {
      const error = new Error('User not found');
      error.statusCode = 404;
      throw error;
    }

    user.status = status;
    user = user.save();
    res
      .status(200)
      .json({
        message: 'Status updated successfully',
        status: user.status
      });
  } catch (error) {
    catchControllerErrors(error, next);
  }
};

// Helper functions
const catchControllerErrors = (error, next) => {
  if (!error.statusCode) {
    error.statusCode = 500;
  }
  next(error);
};

const clearImage = (filePath) => {
  filePath = path.join(__dirname, '..', filePath);
  fs.unlink(filePath, (err) => {
    console.log(err);
  });
};

const getReturnPost = (post, user) => {
  const returnPost = {
    id: post.id,
    title: post.title,
    imageUrl: post.imageUrl,
    content: post.content,
    createdAt: post.createdAt,
    creator: {
      id: user.id,
      name: user.name
    }
  }
  return returnPost;
}