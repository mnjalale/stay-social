const {check, body} = require('express-validator/check');

exports.validatePost = [
  body('title')
    .not().isEmpty().withMessage('Title is required.')
    .isLength({
      min: 5
    }).withMessage('Title length must be at least 5 characters long.'),
  body('content')
    .not().isEmpty().withMessage('Content is required.')
    .isLength({
      min: 5
    }).withMessage('Content length must be at least 5 characters long.')
];

exports.validateUpdate = [
  body('title')
    .not().isEmpty().withMessage('Title is required.')
    .isLength({
      min: 5
    }).withMessage('Title length must be at least 5 characters long.'),
  body('content')
    .not().isEmpty().withMessage('Content is required.')
    .isLength({
      min: 5
    }).withMessage('Content length must be at least 5 characters long.')
];