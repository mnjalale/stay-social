const {body} = require('express-validator/check');
const db = require('../../models/index');
const User = db['User'];

exports.validateSignup = [
  body('name')
    .trim()
    .not().isEmpty().withMessage('Name is required'),
  body('email')
    .trim()
    .not().isEmpty().withMessage('Email is required.')
    .isEmail().withMessage('Enter a valid email')
    .custom((value, {req}) => {
      return User
        .findOne({
          where: {
            email: value
          }
        })
        .then(user => {
          if (user) {
            return Promise.reject('Email address already exists');
          }
        })
    })
    .normalizeEmail(),
  body('password')
    .trim()
    .not().isEmpty().withMessage('Password is required')
    .isLength({
      min: 6
    }).withMessage('Password must be at least 6 characters long.')
    // .custom((value, {req}) => {
    //   if (value != req.body.confirmPassword) {
    //     const error = new Error("Password and confirm password must match");
    //     throw error;
    //   } else {
    //     return true;
    //   }
    // })

];

exports.validateLogin = [
  body('email')
    .trim()
    .not().isEmpty().withMessage('Email is required.')
    .isEmail().withMessage('Enter a valid email')
    .normalizeEmail(),
  body('password')
    .trim()
    .not().isEmpty().withMessage('Password is required.')
]